package jbr.fruits.apple

import jbr.fruits.fruitLib.ExposedObject

class Apple {

  def isFruit: Boolean = {
    true
  }
  
  def fruitType: String = {
    "apple"
  }

  def something() = {
    ExposedObject.doSomething()
  }

}